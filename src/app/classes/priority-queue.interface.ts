

export interface PriorityQueueInterface<T> {

  /**
   * @param priority Ajoute l' élément à la fin de la pile de priorité indiquée
   */
  push(element: T, priority: number): void;

  /**
   * @param priority Concatène les éléments à la pile de priorité indiquée
   */
  pushElements(elements: T[], priority: number): void;

  /**
   * @param priority retourne la liste de priorité indiqué
   */
  getElements(priority: number): T[];

  /**
   * @param priority Dépile la première liste de priorité basse
   */
  popElements(priority?: number): T[];

  /**
   * @param priority Dépile la première liste de priorité haute
   */
  shiftElements(priority?: number): T[];
}
