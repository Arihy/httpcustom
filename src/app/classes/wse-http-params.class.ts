export class WseHttpParams {
  serviceId: string;
  url: string;
  params?: any;
  options?: {
    priority?: number;
    spinner?: boolean;
  };
  method?: string;
}
