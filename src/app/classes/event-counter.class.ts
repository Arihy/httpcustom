import { Observable } from 'rxjs';
import { EventEmitter } from '@angular/core';

/**
 * Se déclenche lorsque tout les évènements ont été déclenchés.
 */
export class EventCounter extends EventEmitter<void> {
  counter = 0;

  constructor() {
    super();
  }

  add(response: Observable<any>): void {
    this.counter++;
    response
      .toPromise()
      .then(() => this.responseReceived())
      .catch(() => this.responseReceived());
  }

  private responseReceived() {
    this.counter--;
    if (this.counter === 0) {
      this.emit();
      console.log('event counter emit');
    }
  }
}
