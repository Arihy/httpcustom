import { EventEmitter } from '@angular/core';
import { WseHttpParams } from './wse-http-params.class';

export class PendingRequest extends EventEmitter<any> {
  priorityChange$: EventEmitter<number>;
  priorityStack: any;
  request: WseHttpParams;

  constructor(priorityStack: any, request: WseHttpParams) {
    super();
    this.priorityChange$ = new EventEmitter();
    this.request = request;
    this.priorityStack = priorityStack;
  }

  changePriority(newPriority: number) {
    this.priorityChange$.emit(newPriority);
  }
}
