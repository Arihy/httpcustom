import { Observable, Subscription } from 'rxjs';

export class Dispatcher {

  private subscription: Subscription;

  public get started(): boolean {
    return !!this.subscription;
  }

  constructor(private trigger: Observable<any>, private action: Function) { }

  start() {
    if (! this.started) {
      this.subscription = this.trigger.subscribe(() => this.action());
    }
  }

  stop() {
    if (this.started) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
  }

}
