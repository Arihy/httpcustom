import { Component } from '@angular/core';
import { WseHttpService } from './services/wse-http.service';
import { PendingRequest } from './classes/pending-request.class';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'devhttp';
  pendingRequestPost: PendingRequest = null;
  pendingRequestGet: PendingRequest = null;

  constructor(public http: WseHttpService) { }

  getData(prio) {
    const request = {
      serviceId: 'app',
      url: '/assets/toto.json',
      options: {
        priority: prio
      },
      method: 'GET'
    };
    this.pendingRequestGet = this.http.request(request);
    this.pendingRequestGet.subscribe((res) => {
      console.log('get ok', prio);
      this.pendingRequestGet = res;
    });
  }

  sendRequests(event: MouseEvent) {
    event.preventDefault();
    for (let i = 0; i < 2; i++) {
      this.postData(1, 500 * i);
    }
    for (let i = 0; i < 2; i++) {
      this.postData(2, 500 * i);
    }
    for (let i = 0; i < 5; i++) {
      this.postData(3, 500 * i);
    }
    for (let i = 0; i < 3; i++) {
      this.postData(1, 500 * i);
    }
  }

  postData(prio, time) {
    const request = {
      serviceId: 'app',
      url: 'data',
      params: {
        priority: prio,
        timeToExcecute: time
      },
      options: {
        priority: prio
      },
      method: 'POST'
    };
    this.pendingRequestPost = this.http.request(request);
    this.pendingRequestPost.subscribe((res) => {
      console.log('post ok', res);
      this.pendingRequestPost = res;
    });
  }

  changePriorityPost() {
    if (this.pendingRequestPost) {
      this.pendingRequestPost.changePriority(1);
    }
  }

  debugHttpQueue(event: MouseEvent) {
    event.preventDefault();
    this.http.debugQueue();
  }
}
