import { Injectable, EventEmitter } from '@angular/core';
import { Observable, BehaviorSubject, merge } from 'rxjs';
import { WseHttpParams } from '../classes/wse-http-params.class';
import { HttpClient } from '@angular/common/http';
import { PendingRequest } from '../classes/pending-request.class';
import { Dispatcher } from '../classes/dispatcher.class';
import { EventCounter } from '../classes/event-counter.class';

export class ProcessingRequests {
  processingPriority: number;
  processingQueue: Array<PendingRequest>;
}

@Injectable()
export class WseHttpService {
  private API_URL = 'http://localhost:8080/api';

  queue: Map<number, PendingRequest[]> = new Map();
  processingRequests$: BehaviorSubject<ProcessingRequests> = new BehaviorSubject(null);

  dispatcher: Dispatcher;
  eventCounter$: EventCounter;
  addedToQueue$: EventEmitter<void> = new EventEmitter();

  constructor(private httpClient: HttpClient) {
    this.eventCounter$ = new EventCounter();
    this.eventCounter$.subscribe(() => this.cleanProcessingRequests());
    const merged = merge(
      this.eventCounter$,
      this.addedToQueue$
    );
    this.dispatcher = new Dispatcher(merged, this.dispatch());
    this.dispatcher.start();
    this.processingRequests$.subscribe((processiongPrequests: ProcessingRequests) => {
      this.onProcessingRequestsChange(processiongPrequests);
    });
  }

  cleanProcessingRequests() {
    this.processingRequests$.next(null);
  }

  /**
   * Permet de traiter la file d'attente en cours de traitement
   * @param processiongPrequests
   */
  onProcessingRequestsChange(processiongPrequests) {
    if (processiongPrequests) {
      this.excecutePendingRequests();
    }
  }

  /**
   * Permet d'ajouter la requete dans la file d'attente
   * @param request
   */
  request(request: WseHttpParams): PendingRequest {
    this.setPriority(request);
    return this.addToQueue(request);
  }

  /**
   * Permet d'assigner une priorité par défaut à la requete si celle si n'en a pas
   * @param request
   */
  setPriority(request: WseHttpParams) {
    if (!request.options) {
      request.options = {
        priority: 1
      };
    } else if (request.options && !request.options.priority) {
      request.options.priority = 1;
    }
  }

  /**
   * Permet d'ajouter une requete dans la queue (fil d'attente)
   * @param request
   */
  addToQueue(request: WseHttpParams): PendingRequest {
    const pendingRequest: PendingRequest = new PendingRequest([], request);
    if (this.queue.get(pendingRequest.request.options.priority)) {
      this.queue.get(pendingRequest.request.options.priority).push(pendingRequest);
    } else {
      this.queue.set(pendingRequest.request.options.priority, [pendingRequest]);
    }
    pendingRequest.priorityChange$.subscribe((newPriority) => {
      // TODO: permettre de changer la priorité de la requete
      console.log('change prio');
    });
    this.addedToQueue$.emit();
    return pendingRequest;
  }

  // TODO:
  updateRequestPriority(id: string, newPriority: number) {
  }

  debugQueue() {
    console.log('queue', this.queue);
    console.log('processing requests', this.processingRequests$.getValue());
  }

  /**
   * Permet d'assigner pile de priorité dans le processing requests pour qu'elle puisse être traité
   */
  dispatch() {
    const self = this;
    return () => {
      const priorities = Array.from(self.queue.keys()).sort();
      if (self.processingRequests$.getValue()) {
        // Si la pile à assigner est de la même priorité que la pile en cours de traitement
        // alors on fusionne la pile à assigner avec la pile en cours de trainement
        if (self.processingRequests$.getValue().processingPriority === priorities[0]) {
          const queueToDispatch = self.queue.get(priorities[0]);
          if (self.queue.delete(priorities[0])) {
            const processingQueue = self.processingRequests$.getValue();
            processingQueue.processingQueue = processingQueue.processingQueue.concat(queueToDispatch);
            self.processingRequests$.next(processingQueue);
          }
        }
      } else {
        // S'il n'y a pas de pile en cours de traitement alors on assigne la pile à assigner pour qu'elle soit traité
        const queueToDispatch = self.queue.get(priorities[0]);
        if (self.queue.delete(priorities[0])) {
          const processingRequest = {
            processingPriority: priorities[0],
            processingQueue: queueToDispatch
          };
          self.processingRequests$.next(processingRequest);
        }
      }
    };
  }

  /**
   * Permet d'executer les requetes qui sont dans la pile en cours de traitement
   */
  excecutePendingRequests() {
    while (this.processingRequests$.getValue().processingQueue.length > 0) {
      const nextRequest = this.processingRequests$.getValue().processingQueue.shift();
      const response = this.excecuteRequest(nextRequest)
      response.subscribe((res) => {
        nextRequest.emit(res);
      });
      this.eventCounter$.add(response);
    }
  }

  /**
   * Permet d'excecuter la requete en attente selon sa méthode POST/GET
   * @param pendingRequest
   */
  excecuteRequest(pendingRequest: PendingRequest): Observable<any> {
    if (pendingRequest.request.method === 'GET') {
      return this.excecuteGetRequest(pendingRequest);
    } else {
      return this.excecutePostRequest(pendingRequest);
    }
  }

  /**
   * Permet de lancer la requete avec la méthode GET
   * @param pendingRequest
   */
  excecuteGetRequest(pendingRequest: PendingRequest): Observable<any> {
    return this.httpClient.get(pendingRequest.request.url);
  }

  /**
   * Permet de lancer la requete avec la méthode POST
   * @param pendingRequest
   */
  excecutePostRequest(pendingRequest: PendingRequest): Observable<any> {
    return this.httpClient.post(`${this.API_URL}/${pendingRequest.request.url}`, pendingRequest.request.params);
  }
}
