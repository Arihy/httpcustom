const express = require('express');
const bodyParser = require('body-parser');
const app = express(); // return an express app

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
  next();
});

app.post('/api/data', (req, res, next) => {
  setTimeout(() => {
    res.json({
      message: `Requete de priorité ${req.body.priority} traité.`
    });
  }, req.body.timeToExcecute);
});

module.exports = app;
